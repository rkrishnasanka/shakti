/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Abhinaya Agrawal
Email ID : agrawal.abhinaya@gmail.com
*/

package Testbench;

// ================================================================

// This module initializes Memory_model and CPU and provides 
// testbench for the Processor. Also establishes the interconnection
// between CPU and MEMORY

// ================================================================
// Bluespec libraries

import Vector       :: *;
import FIFOF        :: *;
import GetPut       :: *;
import ClientServer :: *;
import Connectable  :: *;
import StmtFSM      :: *;

// ================================================================
// Project imports

import TLM2	          :: *;	// Using local copy.
import AHB			  :: *;	// Using local copy. 
import Utils          :: *;
import Req_Rsp        :: *;
import Sys_Configs    :: *;
import C_import_decls :: *;

import ISA_Defs       :: *;
import CPU            :: *;
import Memory_Model   :: *;
import Fabric		  :: *;

`include "TLM.defines"
`include "RVC.defines"
// ================================================================
// Interactive commands from the console
// Commands are returned by the imported C function: c_console_command ()
// which returns a Vector #(10, Bit #(64))
// [0] is the arg count (>= 1)
// [1] is the command, see codes below
// [2..9] are the optional arguments (up to 8 args)

Bit #(32) cmd_continue   = 0;
Bit #(32) cmd_dump       = 1;
Bit #(32) cmd_quit       = 2;
Bit #(32) cmd_reset      = 3;
Bit #(32) cmd_step       = 4;
Bit #(32) cmd_step_until = 5;
Bit #(32) cmd_verbosity  = 6;    // arg: verbosity level
Bit #(32) cmd_dump_mem   = 7;

// ================================================================
// Testbench module

typedef enum {S1, S2, S3} Stages deriving (Eq, Bits);

(* synthesize *)
module mkTestbench (Empty) ;

// CPU interfaces are of type TLM
// CPU has two interfaces - masters
// To recieve these interfaces we must have two AHBMasterXActor
// This will convert the request to AHBFabricMaster that contains an arbiter
// on the other side, we need a AHBSlaveXActor to convert the request back to TLMRecvIFC
// In between, we must have a bus that will connect the master fabric with the slave fabric

// easy
// make Vector #(n, AHBMasterXActor(`TLM_RR_AHB, `TLM_PRM_REQ)) ahb_master <- replicate(mkAHBMaster);

// make Vector #(m, AHBSlaveXActor(`TLM_RR_AHB, `TLM_PRM_RSP)) ahb_slave <- replicate(mkAHBSlave(addr_match));
// Empty ahb_bus <- mkAHBBus (ahb_master.AHBFabricMaster, ahb_slave.AHBFabricSlave);
// make connections between cpu and ahb_master.tlmrecv interface and on the slave side as well


   CPU_IFC        cpu       <- mkCPU_Model;
   Memory_IFC     mem       <- mkMemory_Model;

   mkConnection (cpu.imem_ifc, mem.bus_ifc[0]);
   mkConnection (cpu.dmem_ifc, mem.bus_ifc[1]);

  /* // ----------- Currently not using it -------------------
		// Issue of multiple simultaneous issues is to be resolved
   // AHB Bus Interconnect

	// Helper functions
	function Bool addr_match_mem_instr (AHBAddr #(`TLM_PRM_RSP_MEM) value);
		return ((value >= imem_base_addr && value <= imem_max_addr)?True:False);
	endfunction
	
	function Bool addr_match_mem_data (AHBAddr #(`TLM_PRM_RSP_MEM) value);
		return ((value >= dmem_base_addr && value <= dmem_max_addr)?True:False);
	endfunction
	
	function AHBMasterFabric funct1(AHBMasterActor a);
		return a.fabric;
	endfunction
	
	function AHBSlaveFabric funct2(AHBSlaveActor a);
		return a.fabric;
	endfunction
	
	// Instantaite transactors for TLM Masters and Slaves
	Vector #(Max_Initiators, AHBMasterActor) masterX <- replicateM (mkAHBMaster);

	// Make Slaves
	AHBSlaveActor slave_mem_instr <- mkAHBSlave(addr_match_mem_instr);
	AHBSlaveActor slave_mem_data  <- mkAHBSlave(addr_match_mem_data);

	Vector #(Max_Targets, AHBSlaveActor) slaveX = newVector;
	slaveX[bus_mem_instr_port] = slave_mem_instr;
	slaveX[bus_mem_data_port]  = slave_mem_data;

	// Instantiate bus interface
	Empty ahb_bus <- mkAHBBus (map(funct1, masterX), map(funct2, slaveX));
   
	// Establish connectivity between Bus, Masters and Slaves
	mkConnection(cpu.imem_ifc, masterX[bus_cpu_instr_port].tlm);	// bus_cpu_instr_port = 0
	mkConnection(cpu.dmem_ifc, masterX[bus_cpu_data_port].tlm);		// bus_cpu_data_port  = 1
   
	mkConnection(slaveX[bus_mem_instr_port].tlm, mem.bus_ifc [mem_instr_port]);		// bus_mem_instr_port = 0
    mkConnection(slaveX[bus_mem_data_port].tlm, mem.bus_ifc [mem_data_port]);		// bus_mem_data_port = 1

	rule rl_debug_masters;
		$display($time, " BUS: Master 0: Addr: %h", masterX[0].fabric.bus.hADDR);
		$display($time, " BUS: Master 0: BusREQ %d", masterX[0].fabric.arbiter.hBUSREQ);
		//$display($time, " BUS: Master 0: BusLOCK %d", masterX[0].fabric.arbiter.hLOCK);
		//$display($time, " BUS: Master 0: ");
		$display($time, " BUS: Master 1: Addr: %h", masterX[1].fabric.bus.hADDR);
		$display($time, " BUS: Master 1: BusREQ %d", masterX[1].fabric.arbiter.hBUSREQ);

		$display($time, " BUS: Slave 0: Ready: %d", slaveX[0].fabric.bus.hREADY);
		$display($time, " BUS: Slave 1: Ready: %d", slaveX[1].fabric.bus.hREADY);
	endrule
*/
   // ----------------

   Reg #(Bit #(6)) rg_xj <- mkRegU;

   function Action show64b (String pre, GPR_Data x, String post);
      action
	 $write ("%s", pre);
	 Bool leading = True;
	 for (Integer j = 7; j > 0; j = j - 1) begin
	    if (leading && (x [31:28] == 0))
	       $write (" ");
	    else begin
	       $write ("%1h", x [31:28]);
	       leading = False;
	    end
	    x = x << 4;
	 end
	 $write ("%1h%s", x [31:28], post);
      endaction
   endfunction

   Stmt dump_arch_state =
      seq
	 action
	    let pc      = cpu.read_pc;
	    let instret = cpu.read_instret;
	    let cycle   = cpu.read_cycle;
		$display;
	    $write ("    instret: %0d, CPU cycles: %0d", instret, cycle);
	    show64b ("    PC: ", pc, "\n");
		$display;
	 endaction
	 for (rg_xj <= 0; rg_xj < 32; rg_xj <= rg_xj + 4) seq
	    $write   ("    %2d:", rg_xj);
	    show64b (" ", cpu.read_gpr (truncate (rg_xj)), "");
	    show64b (" ", cpu.read_gpr (truncate (rg_xj) + 1), "");
	    show64b (" ", cpu.read_gpr (truncate (rg_xj) + 2), "");
	    show64b (" ", cpu.read_gpr (truncate (rg_xj) + 3), "\n");
	 endseq
      endseq;

   // ----------------
   // Main behavior: process a queue of interactive commands
   Reg #(Vector #(10, Bit #(32))) rg_console_command <- mkRegU;
   Bit #(32) console_argc     = rg_console_command [0];
   Bit #(32) console_command  = rg_console_command [1];
   Bit #(32) console_arg_1    = rg_console_command [2];
   Bit #(32) console_arg_2    = rg_console_command [3];
   Bit #(32) console_arg_3    = rg_console_command [4];
   Bit #(32) console_arg_4    = rg_console_command [5];
   Bit #(32) console_arg_5    = rg_console_command [6];
   Bit #(32) console_arg_6    = rg_console_command [7];
   Bit #(32) console_arg_7    = rg_console_command [8];
   Bit #(32) console_arg_8    = rg_console_command [9];


   Reg #(Bit #(32)) rg_addr <- mkRegU;
   
   Stmt initialize_cpu
   = seq
	action    // Reset CPU
	   $display ($time, " TB: Initializing CPU");
	   cpu.reset(True);	// True sets mode to debug
	endaction
	
	action    // Set initial PC
	   let start_pc <- c_get_start_pc;
	   cpu.write_pc (truncate (start_pc));
	   $display ($time, " TB: Setting initial PC to: %0h", start_pc);
	endaction
	
	/*
	action	   // Set initial stack pointer in reg x30
	   //cpu.write_gpr (30, 'h8_0000);
	   $display ("Setting initial SP (x30) to: %0h", 'h8_0000);
	endaction*/
   endseq;

   mkAutoFSM (
      seq
	 // Allocate memory, reset the mergesort module
	 action
	    $display ("Testbench: Allocating memory [%0h..%0h]", imem_base_addr, dmem_max_addr);
	    Bool init_from_file = True;
	    mem.initialize (imem_base_addr, dmem_size, init_from_file);
	 endaction

	 initialize_cpu;

	 // Run
	 while (True) seq
	    action
	       let cmd <- c_get_console_command;
	       rg_console_command <= cmd;
	    endaction

	    if (console_command == cmd_continue) seq
	       action
		  Maybe #(GPR_Data) mpc = (  (console_argc == 1)
					   ? tagged Invalid
					   : tagged Valid console_arg_1);
		  cpu.run_continue (mpc);
	       endaction
	       display_stop_reason (" TB: Stop reason: ", cpu.stop_reason, "\n");
	       dump_arch_state;
	    endseq

	    else if (console_command == cmd_dump)
	       dump_arch_state;

	    else if (console_command == cmd_quit)
	       break;

	    else if (console_command == cmd_reset)
	       initialize_cpu;

	    else if (console_command == cmd_step) seq
	       action
		  Maybe #(GPR_Data) mpc = (  (console_argc == 1)
					   ? tagged Invalid
					   : tagged Valid console_arg_1);
		  cpu.run_step (mpc);
	       endaction
	       action
		  display_stop_reason (" TB: Stop reason: ", cpu.stop_reason, "\n");
		  $display ("");
	       endaction
	    endseq

	    else if (console_command == cmd_step_until) seq
	       while ((cpu.read_instret < console_arg_1) && (cpu.stop_reason != CPU_STOP_EXIT))
		  cpu.run_step (tagged Invalid);
	       action
		  display_stop_reason (" TB: Stop reason: ", cpu.stop_reason, "\n");
		  $display ("");
	       endaction
	       dump_arch_state;
	    endseq

	    else if (console_command == cmd_verbosity) action
	       int v = unpack (truncate (console_arg_1));
	       $display ($time, " TB: Setting verbosity to %0d", v);
	       cpu.set_verbosity (v);
	    endaction

	    else if (console_command == cmd_dump_mem)
	       for (rg_addr <= (console_arg_1 & 32'hFFFF_FFFC);
		    rg_addr < console_arg_2;
		    rg_addr <= rg_addr + 4)
	       seq
		  cpu.req_read_memW (rg_addr);
		  action
		     let d <- cpu.rsp_read_memW;
		     $display ($time, " TB: %08h: %08h", rg_addr, d);
		  endaction
	       endseq

	    else
	       $display ("Ignored unknown command: %0d", console_command);
	 endseq
      endseq
      );

   /*Reg #(Stages) stg <- mkReg(S1);

	rule rl_t (stg == S1);
	
		// Allocate memory, reset the mergesort module
		 action
		    $display ("Testbench: Allocating memory [%0h..%0h]", imem_base_addr, dmem_max_addr);
		    Bool init_from_file = True;
		    mem.initialize (imem_base_addr, dmem_size, init_from_file);
		 endaction
		
		 //cpu.reset();
		stg <= S2;
	endrule

	rule rl_tb (stg == S2);
	
		stg <= S3;
	
		action	   // Set initial stack pointer in reg x30
		 //  cpu.write_gpr (10, 'h0_0010);
		   //$display ("Setting initial SP (x30) to: %0h", 'h8_0000);
		endaction

		action    // Set initial PC
		   let start_pc <- c_get_start_pc;
		   cpu.write_pc (truncate (start_pc));
		   $display ("Setting initial PC to: %0h", start_pc);
		endaction

	endrule*/
endmodule

// ================================================================

endpackage: Testbench
