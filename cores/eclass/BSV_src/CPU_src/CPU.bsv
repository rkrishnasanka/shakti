/*
Copyright (c) 2013-2015, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Author Names : Abhinaya Agrawal
Email ID : agrawal.abhinaya@gmail.com
*/

// 07 November 2015
// ### Issues
// 1. Check branch and jump instructions. What happens when processor is flushed but a pending memory request is getting served
// 2. Bus not accessable for two different transaction at the same time. Getting stalled indefinitely
// 3. Operand forwarding from commit stage errs.

// @TODO 
// 1. Add debugger :: Done -- to be tested
// 2. Add Atomic instruction support

package CPU;

// ================================================================

// This package models a RISC-V CPU driving instruction read and
// data read/write traffic onto a fabric.

// ================================================================
// Bluespec libraries

import RegFile      :: *;
import FIFOF        :: *;
import GetPut       :: *;
import ClientServer :: *;
import StmtFSM      :: *;
import DefaultValue :: *;
import SpecialFIFOs :: *;

// ================================================================
// Project imports

import TLM2	   			  :: *;	// Using local copy
import AHB	    		  :: *;	// Using local copy
import Utils       		  :: *;
import Req_Rsp      	  :: *;
import Sys_Configs  	  :: *;
import CPU_Decode_Execute :: *;
import ISA_Defs 	      :: *;

`include "TLM.defines"
`include "RVC.defines"
// ================================================================
// Reasons why the CPU is currently stopped

function Action display_stop_reason (String pre, CPU_Stop_Reason reason, String post);
   action
      $write ($time, pre);
      case (reason)
	 CPU_STOP_BREAK:           $write ("CPU_STOP_BREAK");
	 CPU_STOP_EXIT:            $write ("CPU_STOP_EXIT");
	 CPU_STOP_INSTR_ERR:       $write ("CPU_STOP_INSTR_ERR");
	 CPU_STOP_INSTR_UNIMPLEM:  $write ("CPU_STOP_INSTR_UNIMPLEM");
	 CPU_STOP_MEM_ERR:         $write ("CPU_STOP_MEM_ERR");
      endcase
      $write (post);
   endaction
endfunction

// ================================================================
// CPU interface

interface CPU_IFC;
   // Instruction and Data Memory
   interface TLMSendIFC #(Req_CPU, Rsp_CPU)  imem_ifc;
   interface TLMSendIFC #(Req_CPU, Rsp_CPU)  dmem_ifc;

   // GDB handling
   method Action           			reset (Bool dbg_mode);
   method Action           			run_continue (Maybe #(GPR_Data) mpc);
   method Action           			run_step     (Maybe #(GPR_Data) mpc);
   method Action           			stop;
   method CPU_Stop_Reason  			stop_reason;
   method GPR_Data         			read_pc;
   method Action           			write_pc (GPR_Data d);
   method GPR_Data         			read_gpr (RegName r);
   method Action           			write_gpr (RegName r, GPR_Data d);
   method Action           			req_read_memW (GPR_Data addr);
   method ActionValue #(GPR_Data)	rsp_read_memW ();
   method Action           			write_memW (GPR_Data addr, GPR_Data d);
   method GPR_Data         			read_instret;
   method GPR_Data         			read_cycle;

   // Misc.
   method Action set_verbosity (int verbosity);
endinterface

// ================================================================
// CPU model

typedef enum { CPU_FETCH, CPU_STOPPED } CPU_State deriving (Eq, Bits);

typedef enum { CPU_DBG, CPU_NORM } CPU_Mode deriving (Eq, Bits);

(* synthesize, conflict_free = "rl_commit_1, rl_commit_2" *)
module mkCPU_Model (CPU_IFC);

   // CPU Architectural state
   Reg   #(GPR_Data) rg_pc    <- mkRegU;
   
   // Misc. micro-architectural state
   Reg #(int)              rg_verbosity      <- mkReg(0);
   Reg #(Bool)             rg_stop_requested <- mkReg(False);
   Reg #(CPU_State)        rg_cpu_state      <- mkReg(CPU_STOPPED);
   Reg #(CPU_Mode)	       rg_cpu_mode	     <- mkReg(CPU_DBG);	// To enable processing single instruction at a time while in DBG mode
   Reg #(CPU_Stop_Reason)  rg_stop_reason    <- mkRegU;

   Reg #(Bit #(32))        rg_cycle [2]      <- mkCReg (2, 0);
   Reg #(Bit #(32))        rg_instret        <- mkReg (0);

   // FIFOFs for requests to/responses from memory
   FIFOF #(Req_CPU) f_imem_reqs <- mkBypassFIFOF;
   FIFOF #(Rsp_CPU) f_imem_rsps <- mkBypassFIFOF;

   FIFOF #(Req_CPU) f_dmem_reqs <- mkBypassFIFOF;
   FIFOF #(Rsp_CPU) f_dmem_rsps <- mkBypassFIFOF;

   // Pipeline FIFOFs between stages
   FIFOF #(RegData_Dex) f_if_dex  <- mkPipelineFIFOF;// FIFOF between IF and Decode-Execute Stage.
   FIFOF #(CommitData) 	f_dex_cmt <- mkPipelineFIFOF;// FIFOF between Decode-Execute and Commit Stage.

   // ----------------
   // Misc help-definitions

   Bit #(32) cycle = rg_cycle [1];

   Reg #(Maybe #(GPR_Data)) rg_branch_taken [2] <- mkCReg(2, tagged Invalid);

   //Wire#(Bool) wr_rl_fetch_mkreq_fire <-mkDWire(False);
   Wire #(Bool) wr_flush <- mkDWire(False);

   // ------------
   // Module instantiations

   // Instantiate Decode-Execute stage module
   DecEx_IFC dex <- mkDecEx;   
   
   // ---------
   // Helper functions

   // Helper function to evaluate explicit rule conditions
   function Bool fn_fetch_condition;
	   Bool dbg = ((rg_cpu_mode == CPU_DBG)  && (rg_cpu_state == CPU_FETCH));
	   Bool nrm = ((rg_cpu_mode == CPU_NORM) && (rg_cpu_state == CPU_FETCH));
	   return (dbg || nrm);
   endfunction

   function Bool fn_dex_condition;
	   Bool dbg = (rg_cpu_mode == CPU_DBG)  && (rg_cpu_state != CPU_STOPPED);
	   Bool nrm = (rg_cpu_mode == CPU_NORM) && (rg_cpu_state != CPU_STOPPED);
	   return (dbg || nrm);
   endfunction

   function Bool fn_commit_condition;
	   Bool dbg = (rg_cpu_mode == CPU_DBG)  && (rg_cpu_state != CPU_STOPPED);
	   Bool nrm = (rg_cpu_mode == CPU_NORM) && (rg_cpu_state != CPU_STOPPED);
	   return (dbg || nrm);
   endfunction

   function Bool fn_debug_method_cond;
	   Bool dbg = (rg_cpu_mode == CPU_DBG) && (rg_cpu_state == CPU_STOPPED);
	   return dbg;
   endfunction

   Bool  	   fetch_cond = fn_fetch_condition;
   Bool    	     dex_cond = fn_dex_condition;
   Bool       commit_cond = fn_commit_condition;
   Bool debug_method_cond = fn_debug_method_cond;

   // ----------------
   // BEHAVIOR: Misc

   rule rl_count_cycles (rg_cpu_state != CPU_STOPPED);
      rg_cycle [0] <= rg_cycle [0] + 1;
   endrule

   // ----------------
   // Misc. rules for debugger

   rule cpu_stop (rg_stop_requested == True);
	  rg_cpu_state <= CPU_STOPPED;
   endrule
   
   // =================================================================================
   // BEHAVIOR: FETCH
   // =================================================================================
   //FIFOF #(Bool) request_pending <- mkBypassFIFOF;
   // -------------
   // Setup and send instruction fetch request to memory
   rule rl_fetch_mkreq (fetch_cond);
      Req_Desc_CPU req_desc;
      req_desc = defaultValue;
      req_desc.command    = READ;
      req_desc.data       = ?;
      req_desc.addr       = rg_pc;
      req_desc.burst_size = reqSz_bytes_i (fromInteger(instlen)); // burst_size field for ASZ 32-bits is 2-bits long. b'01 -> 2 bytes of data
	  req_desc.burst_mode = INCR;
	  req_desc.transaction_id = 0;
      Req_CPU req = tagged Descriptor req_desc;

	  f_imem_reqs.enq (req); //request_pending.enq(True);
  	  // wr_rl_fetch_mkreq_fire<= True;

	  if (rg_verbosity > 1)
	 	$display ($time," CPU: Fetch: PC = %0h", rg_pc);
   endrule

/*
   rule rl_1(wr_rl_fetch_mkreq_fire);
	  // Increment PC
	  if (wr_branch_taken matches tagged Valid .v) rg_pc <= v;
	  else  rg_pc <= rg_pc + extend(instlen_byte);
   endrule 
*/
  
   //--------------
   // Assumption - Instructions require no more than one cycle to be fetched
   // Process response and update IF_Dex register
   rule rl_fetch_getrsp (fetch_cond);
      // Read Instruction memory response and store it in if_dex register
	  Rsp_CPU rsp = f_imem_rsps.first;
      f_imem_rsps.deq; //request_pending.deq;

	  RegData_Dex dex_data = RegData_Dex { pc: rg_pc, instr: truncate(rsp.data) };

      f_if_dex.enq(dex_data);

	  if (rg_branch_taken[1] matches tagged Valid .v) begin rg_pc <= v; rg_branch_taken[1] <= tagged Invalid; end
	  else rg_pc <= rg_pc + extend(instlen_byte);
	  
	  //if (rg_verbosity > 1)
		//	report_exec_instr(rsp, fmt)		// implement fmt

	  if(rg_verbosity > 1)	
	  	$display($time," CPU: Fetch: Next Instruction: %h\n\n", rsp.data);
   endrule
   
   // ===================== END: FETCH ===============================================

   
  // =================================================================================
  // BEHAVIOUR: DECODE-EXECUTE
  // =================================================================================

   // ------------
   // BEHAVIOUR: MONITOR INSTRUCTION DECODE-EXECUTION AND READY COMMIT STAGE

   // Rule to setup decode-execute stage
   rule rl_update_dec_ex (dex_cond);
		// Write instruction to DecEx unit
		dex.write_ifr (f_if_dex.first);

		// Write Verbosity to DecEx unit
		dex.write_verbosity (rg_verbosity);

		if (rg_verbosity > 2) begin
			$display($time," CPU: Fetch-Dex: Setting up DEX");	
			$display($time," CPU: Fetch-Dex: PC: %0h, Instruction: %h", rg_pc, f_if_dex.first.instr);
		end
	endrule	

   // Rule to read decode-execute output
   // @TODO: Sort out halt behaviour, Halt at the moment is a dummy instruction to represent end of instruction stream, Current behaviour: $finish
   // BEHAVIOUR: if jump/branch is requested, increment pc, donot enque into dex_commit register, deque if_dex register
   // 			 else if halt was requested, stall the pipeline by not dequing from if_dex register, enque halt req to dex_commit register
   //			 or otherwise, enque into dex_commit register, deque from if_dex register
   rule rl_read_dec_ex (dex_cond);
		Bool hlt_requested = False;
		case (dex.read_cd) matches

			tagged Valid   .v: begin
									if(v matches tagged Halt .hlt) hlt_requested = True;
									f_dex_cmt.enq (v);
							   end

			tagged Invalid .v: begin
							   		rg_stop_reason <= dex.read_stop_reason;
							   		if(rg_verbosity > 1) display_stop_reason(" CPU: Stop/Err Reason: ", dex.read_stop_reason, "\n");
							   end

		endcase
		if(!hlt_requested) f_if_dex.deq;
		if (rg_verbosity > 2) $display($time, " CPU: Dex-Commit: Instruction Executed");
   endrule

   // ----------------
   // ============= END: DECODE-EXECUTE ====================================================
   

   // ======================================================================================
   // BEHAVIOR: COMMIT
   // ======================================================================================
    
   // Rule to commit request
   // Behaviour: if writeback request is recieved, write back to register file using methods provided //
   // 				by dex module, deq from dex_commit register
   //			 else if halt is encountered, implement $finsih, @TODO: sort out this functionality, atleast reset cpu before finish
   //			 else if load/store is encountered, enque request to f_dmem, donot deque from dex_commit register until load/store is completed
   // 			 Note: jump instructions cannot reach commit stage


   rule rl_commit_1 (commit_cond);
		CommitData req = f_dex_cmt.first;
		Bool branch_taken = False;
		Maybe #(GPR_Data) next_pc = tagged Invalid;

		// check for different types of packets
		case (req) matches
			tagged WB    .v: begin dex.write_gpr (v.rd, v.data);
							  f_dex_cmt.deq;
							  $write($time, " CPU: Commit: ");$display("Reg  %d: %h", v.rd, dex.read_gpr (v.rd));
							  if (rg_verbosity > 2) $display($time, " CPU: Commit: Write Back (w/o mem) at: %d with: %h", v.rd, v.data);
							 end
			tagged LS    .v: begin Req_CPU r = v.tlmreq; f_dmem_reqs.enq(r); 
								if (rg_verbosity > 2) $display($time, " CPU: Commit: Load/Store (w/ mem) at: %d addr: %h", v.rd, r.Descriptor.addr);
							 end
			tagged Halt  .v: if (v == True)begin
							    f_dex_cmt.deq;
							   	if(rg_verbosity > 1) display_stop_reason(" CPU: Stop/Err Reason: ", dex.read_stop_reason, "\n");
								$finish();
		                     end
			tagged JMP   .v: begin
					 			f_dex_cmt.deq;
					 			if (v.newpc matches tagged Valid .npc) begin
									next_pc = v.newpc;
							  		if (rg_verbosity > 2) $display($time, " CPU: Commit: Branch Taken New PC: %h", npc);
									if (v.lr matches tagged Valid ._lr) begin
										dex.write_gpr (v.rd, _lr);
							  			if (rg_verbosity > 2) $display($time, " CPU: Commit: Branch Taken New lr: %h", _lr);
									end
									branch_taken = True;
									wr_flush <= True;
								end
								else if (rg_verbosity > 2) $display($time, " CPU: Commit: Branch not taken");
							 end
		endcase
		
		// Update branch taken register
		// Written separately out of the case to validate rg_branch_taken for all cases, ie, when instruction is other than jump
		if(branch_taken == True) rg_branch_taken[0] <= next_pc;
		else rg_branch_taken[0] <= tagged Invalid; 

		// Calculate number of retired instructions
		rg_instret <= rg_instret + 1;
   endrule

   // Rule to process load/store requests
	rule rl_commit_2 (commit_cond);
		let req = f_dex_cmt.first; f_dex_cmt.deq;
		Rsp_CPU rsp = f_dmem_rsps.first; f_dmem_rsps.deq;
		if (req matches tagged LS .reqls) begin	
			GPR_Data temp_data;
			case (reqls.remark)
				UNSIGNED : temp_data = extend(rsp.data);
				SIGNED   : temp_data = signExtend(rsp.data);
				default  : temp_data = extend(rsp.data);
			endcase
			dex.write_gpr (reqls.rd, temp_data);
			if (rg_verbosity > 2) $display($time, " CPU: Commit: Write Back (w/ mem) at: %d with: %h", reqls.rd, temp_data);
		end
	endrule
  
   // Rule to flush all stages
   rule rl_flush (wr_flush);
		f_if_dex.clear;
		f_dex_cmt.clear;
		f_dmem_reqs.clear;
		f_dmem_rsps.clear;
		f_imem_reqs.clear;
		f_imem_rsps.clear;
   endrule
   // ============= END: COMMIT ============================================================
   
   // INTERFACE
   // ----------------
   // Instruction and Data memories
   interface imem_ifc = toSendIFC (f_imem_reqs, f_imem_rsps);
   interface dmem_ifc = toSendIFC (f_dmem_reqs, f_dmem_rsps);


   // =====================================================================================================================
   // ----------------
   // GDB control
   // Work in progress. Not final yet.
   
   method Action reset (Bool dbg_mode) if (rg_cpu_state == CPU_STOPPED);
      rg_cycle [0]		  <= 0;
      rg_instret          <= 0;
      rg_verbosity        <= 3;
	  rg_cpu_mode		  <= dbg_mode?CPU_DBG:CPU_NORM;
	  wr_flush			  <= True;
   endmethod

   method Action run_continue (Maybe #(GPR_Data) mpc) if (debug_method_cond);
      if (mpc matches tagged Valid .new_pc) rg_pc <= new_pc;
      rg_stop_requested <= False;
      rg_stop_reason    <= CPU_STOP_BREAK;
      rg_cpu_state      <= CPU_FETCH;
   endmethod

   method Action run_step (Maybe #(GPR_Data) mpc) if (debug_method_cond);
      if (mpc matches tagged Valid .new_pc) rg_pc <= new_pc;
      rg_stop_requested <= True;
      rg_stop_reason    <= CPU_STOP_BREAK;
      rg_cpu_state      <= CPU_FETCH;
   endmethod

   method Action stop;
      rg_stop_requested <= True;      // Requests stop before next instr
   endmethod

   method CPU_Stop_Reason stop_reason () if (debug_method_cond);
      return rg_stop_reason;
   endmethod

   method GPR_Data read_pc () if (debug_method_cond);
      return rg_pc;
   endmethod

   method Action write_pc (GPR_Data d) if (rg_cpu_mode == CPU_NORM || debug_method_cond);
      rg_pc <= d;
      if (rg_cpu_mode == CPU_NORM) rg_cpu_state <= CPU_FETCH;
   endmethod

   method GPR_Data read_gpr (RegName r) if (debug_method_cond);
	  return dex.read_gpr (r);
   endmethod

   method Action write_gpr (RegName r, GPR_Data d) if (debug_method_cond);
      dex.write_gpr (r, d);
   endmethod


   method Action req_read_memW (GPR_Data addr) if (debug_method_cond);
      Req_Desc_CPU req_desc;
      req_desc = defaultValue;
      req_desc.command    = READ;
      req_desc.data       = ?;
      req_desc.addr       = addr;
      req_desc.burst_size = reqSz_bytes_i (fromInteger(instlen)); // burst_size field for ASZ 32-bits is 2-bits long. b'01 -> 2 bytes of data
	  req_desc.burst_mode = INCR;
	  req_desc.transaction_id = 1;
      Req_CPU req = tagged Descriptor req_desc;

      f_dmem_reqs.enq (req);
   endmethod

   method ActionValue #(GPR_Data) rsp_read_memW () if (debug_method_cond);
      Data d = f_dmem_rsps.first.data; f_dmem_rsps.deq;
	  rg_stop_requested <= True;
      return d;
   endmethod

	method Action write_memW (GPR_Data addr, GPR_Data d) if (debug_method_cond);
	  Req_Desc_CPU req_desc;
      req_desc = defaultValue;
      req_desc.command    = WRITE;
      req_desc.data       = d;
      req_desc.addr       = addr;
      req_desc.burst_size = reqSz_bytes_i (fromInteger(instlen)); // burst_size field for ASZ 32-bits is 2-bits long. b'01 -> 2 bytes of data
	  req_desc.burst_mode = INCR;
	  req_desc.transaction_id = 1;
      Req_CPU req = tagged Descriptor req_desc;

      f_dmem_reqs.enq (req);
	  rg_stop_requested <= True;
   endmethod


   method GPR_Data read_instret if (rg_cpu_state == CPU_STOPPED);
      return rg_instret;
   endmethod


   method GPR_Data read_cycle if (rg_cpu_state == CPU_STOPPED);
      return cycle;
   endmethod

   // ----------------
   // Misc.

   method Action set_verbosity (int verbosity);
      rg_verbosity <= verbosity;
   endmethod

endmodule

// ================================================================

endpackage: CPU
