#######################################3
Usage:

Enter "make" to generate assembly instructions and simulate those instructions using ISS.
To generate random assembly instructions, enter "make generate"
The file assembly.s constitutes assembly instructions
To simulate those instructions using ISS enter "make dump"
To verify the register dump of both ISS and iclass core enter "make verify"

#########################################
