SHAKTI Processor Project
-------------------------

The SHAKTI processor project aims to build variants of processors based on the RISC-V ISA from UC Berkeley (www.riscv.org). 
The project will develop a series of cores, SoC fabrics and a reference SoC for each core family. 
While the cores and most of the SoC components (including bus and interconnect fabrics) will be in open source, some standard components 
like PCIe controller, DDR controller and PHY IP will be proprietary 3rd part IP.

All source will be licensed using a 3 part BSD license and will be royalty and patent free (as far as IIT-Madras is concerned, we will not assert any patents). 
While the primary focus is research, the SoCs are being designed to be competitive with commercial processors with respect to features, silicon area, power profile 
and frequency. This of course assumes that an optimal layout process is used to tape out our design. All FPGA data (Xilinx) will also be made available

While we do plan to tape out a few variants, given the foundry NDA requirements, we will not be able to publish any layout/backend data.


Core Variants
--------------

C class 
-------
1. 32-bit 3-8 stage in-order core aimed at 50-250 Mhz microcontroller variants
2. Optional memory protection 
3. Very low power static design
4. Fault Tolerant variants for ISO26262 applications
5. IoT variants will have compressed/reduced ISA support

I class cores
-------------
1. 64-bit, 1-4 core, 8+ stage out of order, aimed at 200-2 Ghz  industrial control / general purpose applications
2. shared L2 cache, AXI bus, threading support, SIMD/VPU

M Class cores
-------------
1. Enhanced variants of the I-class processors aimed at general purpose compute, low end server and mobile applications
2. Enhancements over I class – larger issue size, quad-threaded, up to 8 cores, freq up to 2.5 Ghz, optional NoC fabric

S class cores
-------------
1. 64-bit superscalar, multi-threaded variant for desktop/server applications.
2. 2-16 cores, crossbar/ring interconnect, segmented L3 cache
3. RapidIO based external cache coherent interconnect for multi-socket applications (up to 256 sockets)
4. Hybrid Memory Cube support    
5. 256/512 bit SIMD
6. Specialized variants with coprocessors for Database acceleration, Security acceleration, Machine Learning
7. Experimental variants will be used as test-bed for our Adaptive System Fabric project which aims to
  design a data-center architecture using NV RAM devices and unified interconnects for memory, storage and networking 
  and leverages persistent memory techniques

H class cores
-------------
1. 64-bit in-order, multi-threaded, HPC variant with 32-128 cores
2. 512 bit SIMD/VPU
3. Goal is 3-5 + Tflops (DP, sustained)

T class cores
-------------
1. Experimental security oriented 64-bit variants with tagged ISA, single address space support, decoupling of protection from memory management.
	
N class cores
-------------
1. Experimental cores for network processing targetting at executing the P4 NP language.
	
SoC Fabrics
-----------
1. AHB style microcontroller fabric
2. AXI style fabric for 1 - 32 cores. Topogies - bus, ring, mesh, NoC
3. Mesh fabric for HPC 


Processor Interconnect
----------------------

We are also developing a processor to processor, cache-coherent interconnect, to allow building of multi-socket S class systems. 
The interconnect is based on the RapidIO standard. We are investigating a two tier scheme where a MOESI/MESIF style scheme is 
used for 2-8 socket systems and a directory based scheme for larger configurations (max 256 sockets)


Design Approach:

The approach is to built optimal (high performance) building blocks that can be shared among the variants and 
then add variant specific IP blocks. The above variants are just canonical references and the Shakti family will
see variants that will be hybrids.


Where  possible, we will also provide the Synopsys/Cadence and Xilinx synthesis results for each module.


Final versions will contain the full BSV code, the generated Verilog code, testbenches, verification IP and FPGA support files.


Related projects
----------------
The SHAKTI effort is part of a larger effort to build complete systems. As part of this effort, IIT-Madras is developing interconnects
(optical and copper) based on Gen 3 (10/25G per lane) RapidIO and a scale-out SSD storage system called lightsor (see lightstor.org) 
based on this interconnect. The final goal is to build a fabric called Adaptive System fabric which will use a combination of 
Hybrid Memory Cubes and RapidIO to  unify support for compute, networking and storage.

Current Status
--------------
An alpha version of the I class with single core has been released publicly

The source repository is composed of 2 directories, the core sources and sources for the non-core IP blocks.
Design documents and standards referenced in the design are provided in the doc ditectory.


Contributors to the Project : 
------------------------------
1. Rahul Bodduna (rahul.bodduna@gmail.com)
2. Neel Gala 	  (neelgala@gmail.com)
3. Arjun Menon    (c.arjunmenon@gmail.com)
4. G S Madhusudan (gs.madhusudan@cse.iitm.ac.in)
5. V. Kamakoti    (veezhi@gmail.com)

For Queries/Collaboration/Feedback :
--------------------------------------

shakti.iitm@gmail.com